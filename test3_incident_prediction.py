import requests
import json


def test_get_incident_prediction_page():
    session = requests.Session()
    url = 'https://stg.insightfinder.com/api/v1/login-check?'
    
    userInfo = {
    'userName': 'oaUser',
    'timezone': 'default',
    'password': 'InsightFinderOA2020!'}
     
    headers = {"User-Agent": "aaaa" } 
    
    resp = session.post(url, params = userInfo, headers=headers, verify=False)
    myCookies = str(session.cookies.get_dict())
    data = resp.json()
    myToken = data["token"]
    url = 'https://stg.insightfinder.com/api/v2/incidentpredictiontimelines?environmentName=All&systemIds=%5B%7B%22id%22%3A%22Trial%20Demo%22%7D%5D&customerName=demoUser&startTime=1604534400000&tzOffset=-18000000'
    url = requests.utils.unquote(url)
    headers = {'User-Agent': "aaaa",
     'cookies': myCookies,
     'Authorization': 'Bearer ' + myToken
     }
    resp = session.get(url, headers=headers, verify=False)
    data = json.loads(resp.content) 
        
    # Validate url is correct, status code is 200 and data is returned in response body
    assert url in requests.utils.unquote(resp.url)
    assert resp.status_code == 200  # this could be done all in one line.
    assert data[0]['id'] == 'Trial Demo' # the url is encoded, I couldn't get it to return a valid response data even though line 33 unquotes the url properly
    
    pageLoadTime = requests.get(url, headers=headers, verify=False).elapsed.total_seconds()
    print("loadtime is " + str(pageLoadTime))