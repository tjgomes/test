import requests
import json

myCookies = ''
myToken = ''

def test_get_login_page():
    
    url = 'https://stg.insightfinder.com/api/v1/login-check?'
    
    userInfo = {
    'userName': 'oaUser',
    'timezone': 'default',
    'password': 'InsightFinderOA2020!'}
     
    headers = {"User-Agent": "aaaa" } 
    
    resp = requests.post(url, params = userInfo, headers=headers, verify=False)
    
    # Validate url is correct and status code is not 400 or 500.
    data = resp.json()
    assert url in resp.url
    assert resp.status_code != (400 or 500)
    assert resp.status_code == 200  # this could be done all in one line.
    assert data['valid'] == True

    pageLoadTime = requests.get(url, params = userInfo, headers=headers, verify=False).elapsed.total_seconds()
    print("loadtime is " + str(pageLoadTime))