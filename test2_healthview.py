import requests
import json
  
def test_get_unified_health_view_trial_demo():
     session = requests.Session()
     url = 'https://stg.insightfinder.com/api/v1/login-check?'
    
     userInfo = {
     'userName': 'oaUser',
     'timezone': 'default',
     'password': 'InsightFinderOA2020!'}
     
     headers = {"User-Agent": "aaaa" } 
    
     resp = session.post(url, params = userInfo, headers=headers, verify=False)
     myCookies = str(session.cookies.get_dict())
     # Validate url is correct and status code is not 400 or 500.
     data = resp.json()
     myToken = data["token"]
     url = 'https://stg.insightfinder.com/api/v2/stats/global/globalviewsystemtimelines?environmentId=All&environmentName=All&customerName=oaUser&systemIds=%5B%7B%22id%22%3A%22Trial%20Demo%22%7D%5D&startTime=1604534400000'
     url = requests.utils.unquote(url)
    
     headers = {'User-Agent': "aaaa",
     'cookies': myCookies,
     'Authorization': 'Bearer ' + myToken
     }
     resp = session.get(url, headers=headers, verify=False)
     data = json.loads(resp.content)
     assert url in requests.utils.unquote(resp.url)
     assert resp.status_code != (400 or 500)
     assert resp.status_code == 200  # this could be done all in one line.
     assert data[0]['id'] == 'Trial Demo'
    
     pageLoadTime = requests.get(url, headers=headers, verify=False).elapsed.total_seconds()
     print("loadtime is " + str(pageLoadTime))
