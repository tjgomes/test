Assuming you have python installed, run the commands below

pip install -U pytest
python -m pip install requests

to run the test *.py files, execute the following commands from a bash command
pytest.exe test1_login.py
pytest.exe test2_healthview.py
pytest.exe test3_incident_prediction.py
pytest.exe test4_root_cause_analysis.py
